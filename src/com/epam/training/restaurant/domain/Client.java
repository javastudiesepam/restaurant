package com.epam.training.restaurant.domain;

import com.epam.training.restaurant.food.FoodFactory;
import com.epam.training.restaurant.food.IFood;

import java.text.DecimalFormat;

public class Client implements EventListener {
  private Double happinessLvl = 1.0;
  private FoodFactory foodFactory;
  private String name;

  public Client(String name, FoodFactory foodFactory) {
    this.foodFactory = foodFactory;
    this.name = name;
  }

  @Override
  public void update(Order order) {
    consumeFood(order.getFood());
  }

  private void consumeFood(IFood food) {
    String happinessLvlPattern = "###,##0.00";
    DecimalFormat df = new DecimalFormat(happinessLvlPattern);

    System.out.println("**********************");
    System.out.println(name + " got his order");
    Double increasedHappinessLvl = food.increaseHappiness(happinessLvl);
    System.out.println("Happiness level is increased from " +
        df.format(happinessLvl) +" to " + df.format(increasedHappinessLvl));
    System.out.println("**********************");
    happinessLvl = increasedHappinessLvl;
  }

  public IFood getUserChoice(Integer choice) {
    return evaluateChoice(choice);
  }

  private IFood evaluateChoice(Integer choice) {
    switch (choice) {
      case 1:
        System.out.println(name + " chose a hot dog");
        return foodFactory.getHotDog(false, false);
      case 2:
        System.out.println(name + " chose a hot dog with ketchup");
        return foodFactory.getHotDog(true, false);
      case 3:
        System.out.println(name + " chose a hot dog with mustard");
        return foodFactory.getHotDog(false, true);
      case 4:
        System.out.println(name + " chose a hot dog with ketchup and mustard");
        return foodFactory.getHotDog(true, true);
      case 5:
        System.out.println(name + " chose chips");
        return foodFactory.getChips(false, false);
      case 6:
        System.out.println(name + " chose chips with ketchup");
        return foodFactory.getChips(true, false);
      case 7:
        System.out.println(name + " chose chips with mustard");
        return foodFactory.getChips(false, true);
      case 8:
        System.out.println(name + " chose chips with ketchup and mustard");
        return foodFactory.getChips(true, true);
      default:
        return null;
    }
  }
}
