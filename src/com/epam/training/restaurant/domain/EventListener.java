package com.epam.training.restaurant.domain;

public interface EventListener {
  void update(Order order);
}
