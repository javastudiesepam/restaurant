package com.epam.training.restaurant.domain;


public class Kitchen {
  private OrdersQueue ordersQueue;
  private EventsPublisher readyOrders;

  public Kitchen(OrdersQueue ordersQueue, EventsPublisher readyOrders) {
    this.ordersQueue = ordersQueue;
    this.readyOrders = readyOrders;
  }

  // run every 5sec
  public void startCooking() {
    Order order = ordersQueue.takeOrder();
    if (order != null) {
      releaseOrder(order);
    } else {
      System.out.println("No orders in the queue");
    }
  }

  private void releaseOrder(Order order) {
    readyOrders.notify(order);
  }
}
