package com.epam.training.restaurant.domain;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class OrdersQueue {
  private Queue<Order> orderQueue = new ConcurrentLinkedQueue<>();

  public void placeOrder(Order order) {
    orderQueue.add(order);
  }

  public Order takeOrder() {
    return orderQueue.poll();
  }
}
