package com.epam.training.restaurant.domain;

public interface EventsPublisher {
  void subscribe(EventListener listener);

  void unsubscribe(EventListener listener);

  void notify(Order order);
}
