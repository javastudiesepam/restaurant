package com.epam.training.restaurant.domain;

import java.util.ArrayList;
import java.util.List;


public class ReadyOrders implements EventsPublisher{
  private List<EventListener> listenerList = new ArrayList<>();

  @Override
  public void subscribe(EventListener listener) {
    listenerList.add(listener);
  }

  @Override
  public void unsubscribe(EventListener listener) {
    listenerList.remove(listener);
  }

  @Override
  public void notify(Order order) {
    Client client = order.getClient();
    for (EventListener listener : listenerList) {
      if (client == listener) {
        listener.update(order);
      }
    }
  }
}
