package com.epam.training.restaurant.domain;

import com.epam.training.restaurant.food.IFood;

public class Order {
  private Client client;
  private IFood food;

  public Order(Client client, IFood food) {
    this.client = client;
    this.food = food;
  }

  public Client getClient() {
    return client;
  }

  public IFood getFood() {
    return food;
  }
}
