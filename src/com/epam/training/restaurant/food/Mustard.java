package com.epam.training.restaurant.food;

public class Mustard extends FoodDecorator implements IFood {
  public Mustard(IFood source) {
    super(source);
  }

  @Override
  public Double increaseHappiness(Double happinessLvl) {
    double happinessModifier = 1.0;
    return (super.increaseHappiness(happinessLvl) * 0) + happinessLvl + happinessModifier;
  }
}
