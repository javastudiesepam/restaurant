package com.epam.training.restaurant.food;

public interface IFood {
  Double increaseHappiness(Double happinessLvl);
}
