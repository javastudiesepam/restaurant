package com.epam.training.restaurant.food;

public class HotDog implements IFood{
  @Override // писать или нет?
  public Double increaseHappiness(Double happinessLvl) {
    Double happinessModifier = 2.0;
    return happinessLvl + happinessModifier;
  }
}
