package com.epam.training.restaurant.food;

public class Chips implements IFood{
  @Override
  public Double increaseHappiness(Double happinessLvl) {
    Double happinessModifier = 1.05;
    return happinessLvl * happinessModifier;
  }
}
