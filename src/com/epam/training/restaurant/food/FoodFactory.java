package com.epam.training.restaurant.food;

public class FoodFactory {
  public IFood getHotDog(Boolean withKetchup, Boolean withMustard) {
    IFood hotDog = new HotDog();
    if (!withKetchup && !withMustard) {
      return hotDog;
    }
    if (withKetchup && !withMustard) {
      return new Ketchup(hotDog);
    }
    if (!withKetchup) {
      return new Mustard(hotDog);
    }
    return new Ketchup(new Mustard(hotDog));
  }

  public IFood getChips(Boolean withKetchup, Boolean withMustard) {
    IFood chips = new Chips();
    if (!withKetchup && !withMustard) {
      return chips;
    }
    if (withKetchup && !withMustard) {
      return new Ketchup(chips);
    }
    if (!withKetchup) {
      return new Mustard(chips);
    }
    return new Ketchup(new Mustard(chips));
  }
}
