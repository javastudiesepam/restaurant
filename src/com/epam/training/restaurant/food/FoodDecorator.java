package com.epam.training.restaurant.food;

public class FoodDecorator implements IFood {
  private IFood wrappee;

  public FoodDecorator(IFood source) {
    wrappee = source;
  }

  @Override
  public Double increaseHappiness(Double happinessLvl) {
    return wrappee.increaseHappiness(happinessLvl);
  }
}
