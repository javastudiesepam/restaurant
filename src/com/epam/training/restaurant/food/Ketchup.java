package com.epam.training.restaurant.food;

public class Ketchup extends FoodDecorator implements IFood {
  public Ketchup(IFood source) {
    super(source);
  }

  @Override
  public Double increaseHappiness(Double happinessLvl) {
    double happinessModifier = 2.0;
    return (super.increaseHappiness(happinessLvl) - happinessLvl) * happinessModifier + happinessLvl;
  }
}
