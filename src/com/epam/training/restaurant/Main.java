package com.epam.training.restaurant;

import com.epam.training.restaurant.domain.*;
import com.epam.training.restaurant.food.FoodFactory;
import com.epam.training.restaurant.food.IFood;
import com.epam.training.restaurant.domain.Client;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

// for the sake of demo
// places random order every 5000 ms
class RandomOrderPlacer implements Runnable{
  private Client client;
  private OrdersQueue ordersQueue;

  RandomOrderPlacer(Client client, OrdersQueue ordersQueue) {
    this.client = client;
    this.ordersQueue = ordersQueue;
  }

  @Override
  public void run() {
    int max;
    int min;
    Random rand = new Random();

    for (int i=0; i < 5; i++) {
      try {
        max = 10000;
        min = 5000;
        int randomTime = rand.nextInt((max - min) + 1) + min;
        Thread.sleep(randomTime);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      max = 8;
      min = 1;
      int randomNum = rand.nextInt((max - min) + 1) + min;

      IFood chosenFood = client.getUserChoice(randomNum);
      Order order = new Order(client, chosenFood);

      ordersQueue.placeOrder(order);
    }
  }
}

class KitchenTimer extends TimerTask {
  private Kitchen kitchen;

  KitchenTimer(Kitchen kitchen) {
    this.kitchen = kitchen;
  }

  @Override
  public void run() {
    kitchen.startCooking();
  }
}

public class Main {

  public static void main(String[] args) {
    FoodFactory foodFactory = new FoodFactory();

    EventsPublisher readyOrders = new ReadyOrders();
    Client client1 = new Client("client 1", foodFactory);
    Client client2 = new Client("client 2", foodFactory);

    readyOrders.subscribe(client1);
    readyOrders.subscribe(client2);

    OrdersQueue ordersQueue = new OrdersQueue();

    Kitchen kitchen = new Kitchen(ordersQueue, readyOrders);
    Timer timer = new Timer();
    timer.schedule(new KitchenTimer(kitchen), 3000, 10000);

    // for test purpose
    Thread thread1 = new Thread(new RandomOrderPlacer(client1, ordersQueue));
    Thread thread2 = new Thread(new RandomOrderPlacer(client2, ordersQueue));

    thread1.start();
    thread2.start();
  }
}
